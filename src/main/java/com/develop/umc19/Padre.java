
package com.develop.umc19;

public class Padre extends persona{
    private String estiloBarba;
    private String colorTes;
    private String tipoVoz;

    public Padre(String nombre, String apellidoP,String apellidoM, int edad,String estiloBarba, String colorTes, String tipoVoz) {
        super(nombre, apellidoP, apellidoM, edad);
        this.estiloBarba = estiloBarba;
        this.colorTes = colorTes;
        this.tipoVoz = tipoVoz;
    }
    public Padre(){
        
    }

    

    public String getEstiloBarba() {
        return estiloBarba;
    }

    public void setEstiloBarba(String estiloBarba) {
        this.estiloBarba = estiloBarba;
    }

    public String getColorTes() {
        return colorTes;
    }

    public void setColorTes(String colorTes) {
        this.colorTes = colorTes;
    }

    public String getTipoVoz() {
        return tipoVoz;
    }

    public void setTipoVoz(String tipoVoz) {
        this.tipoVoz = tipoVoz;
    }
    
    
    
    public String aprenderJava(){
        return"El padre está prenddiendo java";
    }
    
    public void manejandoAuto(){
        System.out.println("se manejar auto");
    }
    
}
