

package com.develop.umc19;

public class alumno extends persona {
    private int matricula;
    private double calificacion;

    public alumno() {
        super("jose", "becerra", "nose", 20);
        
    }
    public alumno(int matricula, double calificacion) {
        this.matricula=matricula;
        this.calificacion=calificacion;
        
    }

    public alumno(String nombre, String apellido_Paterno, String apellido_Materno, int edad,int matricula, double calificacion) {
        super(nombre, apellido_Paterno, apellido_Materno, edad);
        this.matricula=matricula;
        this.calificacion=calificacion;
    }
    
    

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }
    
    public String verDetalleAlumno(){
        
        return nombre + "  "+apellido_Paterno+" " + apellido_Materno +"  "+edad+"  "+matricula+"  "+calificacion;
    }

}
